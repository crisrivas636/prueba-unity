using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
[System.Serializable]
public class Card
{
    public int id;
    public string color;
    public string typeOfCard;
    public string description;

    public Sprite thisImage;

    public Card()
    {

    }

    public Card(int Id, string Color, string TypeOfCard, string Description, Sprite ThisImage)
    {
        id = Id;
        color = Color;
        typeOfCard = TypeOfCard;
        description = Description;

        thisImage = ThisImage;

    }
}