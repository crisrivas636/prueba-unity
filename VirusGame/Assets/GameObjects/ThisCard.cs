using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ThisCard : MonoBehaviour
{
    public List<Card> thisCard = new List<Card>();
    public int thisId;

    public int id;
    public string color;
    public string typeOfCard;
    public string cardDescription;

    public Text descriptionText;
    public Text idText;

    public Sprite thisSprite;
    public Image thatImage;

    void Start()
    {
        thisCard[0] = CardDataBase.cardList[thisId];    
    }

    void Update()
    {
        id = thisCard[0].id;
        cardDescription = thisCard[0].description;
        thisSprite = thisCard[0].thisImage;

        descriptionText.text = " " + cardDescription;
        idText.text = " " + id;
        thatImage.sprite = thisSprite;

    }
}
