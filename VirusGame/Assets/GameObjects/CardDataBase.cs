using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardDataBase : MonoBehaviour
{
    public static List<Card> cardList = new List<Card>();

    void Awake()
    {
        cardList.Add(new Card(0, "none", "none", "none", Resources.Load <Sprite>("1") ));
        cardList.Add(new Card(1, "Green", "Organ", "Liver", Resources.Load<Sprite>("organGreen")));
        cardList.Add(new Card(2, "Blue", "Organ", "Brain", Resources.Load<Sprite>("organBlue")));
        cardList.Add(new Card(3, "Orange", "Organ", "Bone", Resources.Load<Sprite>("organOrange")));
        cardList.Add(new Card(4, "Red", "Organ", "Hearth", Resources.Load<Sprite>("organRed")));
        cardList.Add(new Card(5, "Multicolor", "Organ", "MultiOrgan", Resources.Load<Sprite>("organMulticolor")));
        cardList.Add(new Card(6, "Green", "Medicine", "M.Green", Resources.Load<Sprite>("medicineGreen")));
        cardList.Add(new Card(7, "Blue", "Medicine", "M.blue", Resources.Load<Sprite>("medicineBlue")));
        cardList.Add(new Card(8, "Orange", "Medicine", "M.Orange", Resources.Load<Sprite>("medicineOrange")));
        cardList.Add(new Card(9, "Red", "Medicine", "M.Red", Resources.Load<Sprite>("medicineRed")));
        cardList.Add(new Card(10, "Multicolor", "Medicine", "M.Multi", Resources.Load<Sprite>("medicineMulticolor")));
        cardList.Add(new Card(11, "Green", "Virus", "Virus", Resources.Load<Sprite>("virusGreen")));
        cardList.Add(new Card(12, "Blue", "Virus", "Virus", Resources.Load<Sprite>("virusBlue")));
        cardList.Add(new Card(13, "Orange", "Virus", "Virus", Resources.Load<Sprite>("virusOrange")));
        cardList.Add(new Card(14, "Red", "Virus", "Virus", Resources.Load<Sprite>("virusRed")));
        cardList.Add(new Card(15, "Multicolor", "Virus", "MultiVirus", Resources.Load<Sprite>("virusMulticolor")));
        cardList.Add(new Card(16, "Special", "Special", "totalChange", Resources.Load<Sprite>("totalChange")));
        cardList.Add(new Card(17, "Special", "Special", "robber", Resources.Load<Sprite>("robber")));
        cardList.Add(new Card(18, "Special", "Special", "lowerCards", Resources.Load<Sprite>("lowerCards")));
        cardList.Add(new Card(19, "Special", "Special", "infection", Resources.Load<Sprite>("infection")));
        cardList.Add(new Card(20, "Special", "Special", "changeOrgan", Resources.Load<Sprite>("changeOrgan")));
    }
}
